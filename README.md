# Drew Stinnett 👋

- 🔭 I’m currently working at Duke University
- 📫 You can find me on twitter at [@BrewerDrewer](https://twitter.com/BrewerDrewer)
- 😄 Pronouns: he/him/his
- ⚡ Fun fact: Can you spot me in [this](https://www.youtube.com/watch?v=oL9WnB0qHBA)?
- 🎙 Cohost of the weekly podcast [Imminent Teachnology](https://podcast.imminentteachnology.com/) about Technology and Inclusion with [Dr. Rochelle Newton](https://www.linkedin.com/in/drrochellenewton/). Wanna be a guest? Hit me up!

##   Recently Created Projects

- [goreleaser-ci-test](https://gitlab.com/drewstinnett/goreleaser-ci-test) 2 years ago
- [Hello Duke Friends](https://gitlab.com/welcome.php/hello-duke-friends) 2 years ago
- [test-template](https://gitlab.com/drewstinnett/test-template) 2 years ago
- [drewstinnett](https://gitlab.com/drewstinnett/drewstinnett) 2 years ago
- [goreleaser-project](https://gitlab.com/drewstinnett/goreleaser-project) 2 years ago

## 🎞 Recently Watched Films

- Rewatched [The Fate of the Furious, 2017 - ★★★★★](https://letterboxd.com/mondodrew/film/the-fate-of-the-furious/4/) 12 hours ago
- Rewatched [Brawl in Cell Block 99, 2017 - ★★★](https://letterboxd.com/mondodrew/film/brawl-in-cell-block-99/) 15 hours ago
- Rewatched [Indiana Jones and the Dial of Destiny, 2023 - ★★★½](https://letterboxd.com/mondodrew/film/indiana-jones-and-the-dial-of-destiny/1/) 20 hours ago
- Rewatched [Furious 7, 2015 - ★★★★](https://letterboxd.com/mondodrew/film/furious-7/5/) 1 day ago
- Watched [One More Shot, 2024 - ★★½](https://letterboxd.com/mondodrew/film/one-more-shot-2024/) 1 day ago