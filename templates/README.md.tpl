# Drew Stinnett 👋

- 🔭 I’m currently working at Duke University
- 📫 You can find me on twitter at [@BrewerDrewer](https://twitter.com/BrewerDrewer)
- 😄 Pronouns: he/him/his
- ⚡ Fun fact: Can you spot me in [this](https://www.youtube.com/watch?v=oL9WnB0qHBA)?
- 🎙 Cohost of the weekly podcast [Imminent Teachnology](https://podcast.imminentteachnology.com/) about Technology and Inclusion with [Dr. Rochelle Newton](https://www.linkedin.com/in/drrochellenewton/). Wanna be a guest? Hit me up!

##   Recently Created Projects
{{ range gitlabRecentlyCreatedProjects 5 }}
- [{{ .Name }}]({{ .WebURL }}) {{ .CreatedAt | builtinAgo }}
{{- end }}

## 🎞 Recently Watched Films
{{ range letterboxdRecentlyWatched 5 }}
- {{ .Verb }} [{{ .TitleWithRating }}]({{ .Link }}) {{ .PubDate | builtinAgo }}
{{- end }}